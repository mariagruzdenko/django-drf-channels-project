from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from server.views import ServerListViewSet, CategoryListViewSet, ServerMembershipViewSet
from webchat.consumer import WebChatConsumer
from webchat.views import MessageViewSet
from account.views import (
    AccountViewSet,
    JWTCookieTokenRefreshView,
    JWTCookieTokenObtainPairView,
    LogOutAPIView,
    RegisterAPIView,
)


router = DefaultRouter()
router.register("api/server/select", ServerListViewSet)
router.register("api/server/category", CategoryListViewSet)
router.register("api/messages", MessageViewSet, basename="messages")
router.register("api/account", AccountViewSet, basename="account")
router.register(
    r"api/membership/(?P<server_id>\d+)/membership",
    ServerMembershipViewSet,
    basename="membership",
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "api/token/", JWTCookieTokenObtainPairView.as_view(), name="token_obtain_pair"
    ),
    path(
        "api/token/refresh/", JWTCookieTokenRefreshView.as_view(), name="token_refresh"
    ),
    path("api/logout/", LogOutAPIView.as_view(), name="logout"),
    path("api/register/", RegisterAPIView.as_view(), name="register"),
    path("api/docs/schema/", SpectacularAPIView.as_view(), name="schema"),
    path("api/docs/schema/ui/", SpectacularSwaggerView.as_view()),
] + router.urls

websocket_urlpatterns = [
    path("<str:serverId>/<str:channelId>", WebChatConsumer.as_asgi()),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
