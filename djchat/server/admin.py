from django.contrib import admin

from .models import Channel, Server, Category

admin.site.register(Category)
admin.site.register(Server)
admin.site.register(Channel)
