import { Route, Routes, BrowserRouter } from 'react-router-dom';

import Home from './pages/Home';
import MainServer from './pages/Server';
import Explore from './pages/Explore';
import ToggleColorMode from './components/ToggleColorMode';
import Login from './pages/Login';
import { AuthServiceProvider } from './context/AuthContext';
import Register from './pages/Register';
import MembershipProvider from './context/MemberContext';
import ProtectedRoute from './services/ProtectedRoute';
import MembershipCheck from './components/Membership/MembershipCheck';

const App = () => {
  return (
    <BrowserRouter>
      <AuthServiceProvider>
        <ToggleColorMode>
          <Routes>
            <Route path='/' element={<Home />} />
            <Route
              path='/server/:serverId/:channelId?'
              element={
                <ProtectedRoute>
                  <MembershipProvider>
                    <MembershipCheck>
                      <MainServer />
                    </MembershipCheck>
                  </MembershipProvider>
                </ProtectedRoute>
              }
            />
            <Route path='/explore/:categoryName' element={<Explore />} />
            <Route path='/login' element={<Login />} />
            <Route path='/register' element={<Register />} />
          </Routes>
        </ToggleColorMode>
      </AuthServiceProvider>
    </BrowserRouter>
  );
};

export default App;
