import React from 'react';
import { useParams } from 'react-router-dom';
import {
  Box,
  Typography,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  TextField,
} from '@mui/material';
import { useTheme } from '@mui/material/styles';

import { Server } from '../../@types/server.d';
import MessageInterfaceChannels from '../Main/MessageInterfaceChannels';
import Scroll from './Scroll';
import useWebChatSocket from '../../services/ChatService';
import useMembership from '../../services/MembershipService';

interface SendMessageData {
  type: string;
  message: string;
  [key: string]: any;
}

interface ServerChannelProps {
  data: Server[];
}

interface Message {
  sender: string;
  content: string;
  timestamp: string;
}

const MessageInterface = (props: ServerChannelProps) => {
  const { data } = props;
  const theme = useTheme();

  const server_name = data?.[0]?.name ?? 'server';
  const { serverId, channelId } = useParams();

  const { newMessage, message, setMessage, sendJsonMessage } = useWebChatSocket(
    channelId || '',
    serverId || '',
  );

  const { isUserMember } = useMembership();

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      sendJsonMessage({
        type: 'message',
        message,
      } as SendMessageData);
    }
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    sendJsonMessage({
      type: 'message',
      message,
    } as SendMessageData);
  };

  function formatTimeStamp(timestamp: string) {
    const date = new Date(Date.parse(timestamp));
    const formattedDate = `
    ${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
    const formattedTime = date.toLocaleTimeString([], {
      hour: '2-digit',
      minute: '2-digit',
      hour12: true,
    });
    return `${formattedDate} at ${formattedTime}`;
  }

  return (
    <>
      <MessageInterfaceChannels data={data} />
      {channelId == undefined ? (
        <Box
          sx={{
            overflow: 'hidden',
            p: { xs: 0 },
            height: `calc(80vh)`,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Box sx={{ textAlign: 'center' }}>
            <Typography
              variant='h4'
              fontWeight={700}
              letterSpacing={'-0.5px'}
              sx={{ px: 5, maxWidth: '600px' }}
            >
              Welcome to {server_name}
            </Typography>
            <Typography>
              {data?.[0]?.description ?? 'This is our home'}
            </Typography>
          </Box>
        </Box>
      ) : (
        <>
          <Box sx={{ overflow: 'hidden', p: 0, height: `calc(100vh - 100px)` }}>
            <Scroll>
              <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
                {newMessage.map((msg: Message, index: number) => {
                  return (
                    <ListItem key={index} alignItems='flex-start'>
                      <ListItemAvatar>
                        <Avatar alt='user image' />
                      </ListItemAvatar>
                      <ListItemText
                        primaryTypographyProps={{
                          fontSize: '12px',
                          variant: 'body2',
                        }}
                        primary={
                          <React.Fragment>
                            <Typography
                              component='span'
                              variant='body1'
                              color='text.primary'
                              sx={{ display: 'inline', fontWeight: 600 }}
                            >
                              {msg.sender}
                            </Typography>

                            <Typography
                              component='span'
                              variant='caption'
                              color='textSecondary'
                            >
                              {' at '} {formatTimeStamp(msg.timestamp)}
                            </Typography>
                          </React.Fragment>
                        }
                        secondary={
                          <React.Fragment>
                            <Typography
                              variant='body1'
                              style={{
                                overflow: 'visible',
                                whiteSpace: 'normal',
                                textOverflow: 'clip',
                              }}
                              sx={{
                                dislpay: 'inline',
                                lineHeight: 1.2,
                                fontWeight: 400,
                                letterSpacing: '-0.2px',
                              }}
                              color='text.primary'
                              component='span'
                            >
                              {msg.content}
                            </Typography>
                          </React.Fragment>
                        }
                      ></ListItemText>
                    </ListItem>
                  );
                })}
              </List>
            </Scroll>
          </Box>
          <Box sx={{ position: 'sticky', bottom: 0, width: '100%' }}>
            <form
              onSubmit={handleSubmit}
              style={{
                bottom: 0,
                right: 0,
                padding: '1rem',
                backgroundColor: theme.palette.background.default,
                zIndex: 1,
              }}
            >
              {isUserMember ? (
                <Box sx={{ display: 'flex' }}>
                  <TextField
                    fullWidth
                    multiline
                    minRows={1}
                    maxRows={4}
                    sx={{ flexGrow: 1 }}
                    value={message}
                    onKeyDown={handleKeyDown}
                    onChange={(e) => setMessage(e.target.value)}
                  />
                </Box>
              ) : (
                ''
              )}
            </form>
          </Box>
        </>
      )}
    </>
  );
};
export default MessageInterface;
