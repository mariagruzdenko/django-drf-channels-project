import {
  AppBar,
  ListItemAvatar,
  Toolbar,
  Typography,
  Avatar,
  Box,
  IconButton,
  Drawer,
  useMediaQuery,
} from '@mui/material';
import { MoreVert } from '@mui/icons-material';
import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';

import { Server } from '../../@types/server.d';
import { useTheme } from '@mui/material';
import { MEDIA_URL } from '../../config';
import ServerChannels from '../SecondaryDraw/ServerChannels';
import JoinServerButton from '../Membership/JoinServerButton';

interface ServerChannelProps {
  data: Server[];
}

const MessageInterfaceChannels = (props: ServerChannelProps) => {
  const { data } = props;
  const theme = useTheme();
  const { serverId, channelId } = useParams();
  const [sideMenu, setSideMenu] = useState(false);
  const isSmallScreen = useMediaQuery(theme.breakpoints.up('sm'));
  const channelName =
    data
      ?.find((server) => server.id == Number(serverId))
      ?.channel_server?.find((channel) => channel.id === Number(channelId))
      ?.name || 'home';

  useEffect(() => {
    if (isSmallScreen && sideMenu) {
      setSideMenu(false);
    }
  }, [isSmallScreen]);

  const toggleDrawer =
    (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }
      setSideMenu(open);
    };

  const list = () => (
    <Box
      sx={{ paddingTop: `${theme.primaryAppBar.height}px`, minWidth: 200 }}
      onClick={toggleDrawer(false)}
      onKeyDown={toggleDrawer(false)}
    >
      <ServerChannels data={data} />
    </Box>
  );
  return (
    <>
      <AppBar
        sx={{
          backgroundColor: theme.palette.background.default,
          borederBottom: `1px solid ${theme.palette.divider}`,
        }}
        color='default'
        position='sticky'
        elevation={0}
      >
        <Toolbar
          variant='dense'
          sx={{
            minHeight: theme.primaryAppBar.height,
            height: theme.primaryAppBar.height,
            dispaly: 'flex',
            alignItems: 'center',
          }}
        >
          <Box sx={{ display: { xs: 'block', sm: 'none' } }}>
            <ListItemAvatar sx={{ minWidth: '40px' }}>
              <Avatar
                alt='Server Icon'
                src={`${MEDIA_URL}${data?.[0]?.icon}`}
                sx={{ width: 30, height: 30 }}
              />
            </ListItemAvatar>
          </Box>
          <Typography noWrap component='div'>
            {channelName}
          </Typography>
          <Box sx={{ flexGrow: 1 }}></Box>

          <JoinServerButton />
          <Box sx={{ display: { xs: 'block', sm: 'none' } }}>
            <IconButton color='inherit' edge='end' onClick={toggleDrawer(true)}>
              <MoreVert />
            </IconButton>
          </Box>
          <Drawer anchor='left' open={sideMenu} onClose={toggleDrawer(false)}>
            {list()}
          </Drawer>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default MessageInterfaceChannels;
