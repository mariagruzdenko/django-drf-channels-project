import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Button } from '@mui/material';

import { useMembershipContext } from '../../context/MemberContext';

const JoinServerButton = () => {
  const navigate = useNavigate();
  const { serverId } = useParams();
  const { joinServer, leaveServer, isLoading, error, isUserMember } =
    useMembershipContext();

  const handleJoinServer = async () => {
    try {
      await joinServer(Number(serverId));
      console.log('User has joined the server.', error);
    } catch (error) {
      console.log('Error joining.');
    }
  };

  const handleLeaveServer = async () => {
    try {
      await leaveServer(Number(serverId));
      navigate(`/server/${serverId}/`);
      console.log('User has left the server successfully!');
    } catch (error) {
      console.error('Error leaving the server:');
    }
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <>
      ismember: {isUserMember.toString()}
      {isUserMember ? (
        <Button variant='contained' onClick={handleLeaveServer}>
          Leave Server
        </Button>
      ) : (
        <Button variant='contained' onClick={handleJoinServer}>
          Join Server
        </Button>
      )}
    </>
  );
};

export default JoinServerButton;
