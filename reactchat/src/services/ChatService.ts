import useWebSocket from 'react-use-websocket';
import { useState } from 'react';

import useCrud from '../hooks/useCrud';
import { useAuthService } from '../services/AuthServices';
import { WS_ROOT } from '../config';
import { Server } from '../@types/server.d';

interface Message {
  sender: string;
  content: string;
  timestamp: string;
}

const useWebChatSocket = (channelId: string, serverId: string) => {
  const [newMessage, setNewMessage] = useState<Message[]>([]);
  const [message, setMessage] = useState('');
  const { logout, refreshAccessToken } = useAuthService();
  const { fetchData } = useCrud<Server>(
    [],
    `/messages/?channel_id=${channelId}`,
  );

  const socketUrl = channelId ? `${WS_ROOT}/${serverId}/${channelId}` : null;

  const [reconnectAttempt, setReconnectionAttemt] = useState(0);
  const maxReconnectionAttempts = 4;

  const { sendJsonMessage } = useWebSocket(socketUrl, {
    onOpen: async () => {
      try {
        const data = await fetchData();
        setNewMessage([]);
        setNewMessage(Array.isArray(data) ? data : []);
        console.log('Connected!!!');
      } catch (error) {
        console.log(error);
      }
    },
    onClose: (event: CloseEvent) => {
      if (event.code === 4001) {
        console.log('Authentication Error!');
        refreshAccessToken().catch((error) => {
          if (error.response && error.response.status === 401) {
            logout();
          }
        });
      }
      console.log('Closed!');
      setReconnectionAttemt((previousAttempt) => previousAttempt + 1);
    },
    onError: () => {
      console.log('Error!');
    },
    onMessage: (msg) => {
      const data = JSON.parse(msg.data);
      setNewMessage((prev_msg) => [...prev_msg, data.new_message]);
      setMessage('');
    },
    shouldReconnect: (closeEvenet: CloseEvent) => {
      if (
        closeEvenet.code === 4001 &&
        reconnectAttempt >= maxReconnectionAttempts
      ) {
        setReconnectionAttemt(0);
        return false;
      }
      return true;
    },
    reconnectInterval: 1000,
  });

  return {
    newMessage,
    message,
    setMessage,
    sendJsonMessage,
  };
};

export default useWebChatSocket;
