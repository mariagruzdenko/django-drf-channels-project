import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

import { AuthServiceProps } from '../@types/auth-service';
import { BASE_URL } from '../config';

export function useAuthService(): AuthServiceProps {
  const navigate = useNavigate();
  const getInitialLoggedInValue = () => {
    const loggedIn = localStorage.getItem('isLoggedIn');
    return loggedIn !== null && loggedIn === 'true';
  };

  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(
    getInitialLoggedInValue(),
  );

  const getUserDetails = async () => {
    try {
      const userId = localStorage.getItem('userId');
      const response = await axios.get(
        `http://127.0.0.1:8000/api/account/${userId}`,
        { withCredentials: true },
      );
      const userDetails = response.data;
      localStorage.setItem('username', userDetails.username);
      setIsLoggedIn(true);
    } catch (err: any) {
      setIsLoggedIn(false);
      return err;
    }
  };

  const login = async (username: string, password: string) => {
    try {
      const response = await axios.post(
        'http://127.0.0.1:8000/api/token/',
        {
          username,
          password,
        },
        { withCredentials: true },
      );
      const userId = response.data.user_id;
      localStorage.setItem('isLoggedIn', 'true');
      localStorage.setItem('userId', userId);
      setIsLoggedIn(true);

      getUserDetails();
    } catch (err: any) {
      return err.response.status;
    }
  };

  const register = async (username: string, password: string) => {
    try {
      const response = await axios.post(
        'http://127.0.0.1:8000/api/register/',
        {
          username,
          password,
        },
        { withCredentials: true },
      );
      return response.status;
    } catch (err: any) {
      return err.response.status;
    }
  };

  const refreshAccessToken = async () => {
    try {
      await axios.post(
        `${BASE_URL}/token/refresh/`,
        {},
        { withCredentials: true },
      );
    } catch (refreshError) {
      return Promise.reject(refreshError);
    }
  };

  const logout = async () => {
    localStorage.setItem('isLoggeIn', 'false');
    localStorage.removeItem('user_id');
    localStorage.removeItem('username');
    setIsLoggedIn(false);
    navigate('/login');

    try {
      await axios.post(`${BASE_URL}/logout/`, {}, { withCredentials: true });
    } catch (error) {
      return Promise.reject(error);
    }
  };
  return { login, isLoggedIn, logout, refreshAccessToken, register };
}
